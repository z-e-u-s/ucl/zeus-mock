import asyncio
from typing import List

import uvicorn
from fastapi import FastAPI, HTTPException

from zeus_mock.zeus_mock import ZeusMock

zeus_api = FastAPI()


class Api:
    @zeus_api.post("/")
    async def api(data: List[dict]):
        results_list = []
        for json_test_definition in data:
            zeus_mock = ZeusMock(json_test_definition)
            if not zeus_mock.check_opsensearch_connection():
                raise HTTPException(
                    status_code=502, detail="OpenSearch is not available"
                )
            valid_data, error = zeus_mock.validate_data()
            if not valid_data:
                result = {
                    "execution_id": zeus_mock.execution_id,
                    "data_validation": f"faulty - {error}",
                    "status": "OpenSearch connection establised but data not sent",
                }
            else:
                loop = asyncio.get_event_loop()
                loop.create_task(zeus_mock.send_files_to_opensearch())
                result = {
                    "(Mock only) Logprep running with Pod name of execution_id": zeus_mock.execution_id,
                    "data_validation": "ok",
                    "status": "will be sent to OpenSearch",
                    "sre_to_create": zeus_mock.sre_to_create,
                    "used_delay in (s)": zeus_mock.delay,
                    "validated_input": zeus_mock._data_json_dict,
                }
            results_list.append(result)
        return results_list

    def run_api():
        uvicorn.run(zeus_api, host="0.0.0.0", port=8000)
