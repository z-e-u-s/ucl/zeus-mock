import asyncio
import json
import math
import os
import random
import uuid
import warnings
from datetime import datetime

import requests
import urllib3
from dotenv import load_dotenv

load_dotenv()


class ZeusMock:

    _data_json_string = None
    _data_json_dict = None
    _data_delay = None
    _data_sre_to_create = None
    _execution_id = None
    _rule_id = None
    _opensearch_destination_host = os.environ.get(
        "OPENSEARCH_HOST", "https://opensearch"
    )
    _opensearch_destination_port = os.environ.get("OPENSEARCH_PORT", "9200")
    _opensearch_destination_sre_index = os.environ.get("OPENSEARCH_SRE_INDEX", "sre")
    _opensearch_destination_event_index = os.environ.get(
        "OPENSEARCH_EVENT_INDEX", "event"
    )
    _open_search_user_name = os.getenv("OPENSEARCH_USER_NAME", "admin")
    _open_search_user_password = os.getenv("OPENSEARCH_USER_PASSWORD", "admin")

    def __init__(self, data):
        self._data_json_dict = data

    def validate_data(self):
        try:
            json.dumps(self._data_json_dict)
        except Exception:
            return False, "invlaid json"
        if "mock" not in self._data_json_dict:
            self._data_json_dict["mock"] = {}
        if "sre_to_create" not in self._data_json_dict["mock"]:
            self._data_json_dict["mock"]["sre_to_create"] = random.randint(1, 2)
        if "delay" not in self._data_json_dict["mock"]:
            self._data_json_dict["mock"]["delay"] = random.randint(1, 15)
        if type(self._data_json_dict["mock"]["sre_to_create"]) is int:
            self._data_sre_to_create = self._data_json_dict["mock"]["sre_to_create"]
        else:
            return False, "sre_to_create is not an integer"
        try:
            math.isnan(self._data_json_dict["mock"]["delay"])
        except Exception:
            return False, "delay is not a number"
        self._data_delay = float(self._data_json_dict["mock"]["delay"])
        if "execution_id" not in self._data_json_dict:
            return False, "execution_id is required"
        self._execution_id = self._data_json_dict["execution_id"]
        if "rule_id" not in self._data_json_dict:
            return False, "rule_id is required"
        self._rule_id = self._data_json_dict["rule_id"]
        if "test_events" not in self._data_json_dict:
            return False, "Test is reqired"
        if type(self._data_json_dict["test_events"]) is not list:
            return False, "Test has to be a list"
        return True, ""

    async def send_files_to_opensearch(self):
        opensearch_destination = (
            f"{self._opensearch_destination_host}:"
            f"{self._opensearch_destination_port}/"
        )
        for test_object in self._data_json_dict["test_events"]:
            await asyncio.sleep(self._data_delay)
            if self._data_sre_to_create == 0:
                predetector_id = str(uuid.uuid4())
                with warnings.catch_warnings():
                    warnings.filterwarnings(
                        "ignore", category=urllib3.exceptions.InsecureRequestWarning
                    )
                    event = self._return_event(test_object, predetector_id)
                    requests.post(
                        (
                            f"{opensearch_destination}"
                            f"{self._opensearch_destination_event_index}/_doc"
                        ),
                        json=event,
                        auth=(
                            self._open_search_user_name,
                            self._open_search_user_password,
                        ),
                        verify=False,
                    )
            else:
                for sre_count in range(self._data_sre_to_create):
                    predetector_id = str(uuid.uuid4())
                    with warnings.catch_warnings():
                        warnings.filterwarnings(
                            "ignore", category=urllib3.exceptions.InsecureRequestWarning
                        )
                        sre = self._return_sre(predetector_id)
                        requests.post(
                            (
                                f"{opensearch_destination}"
                                f"{self._opensearch_destination_sre_index}/_doc"
                            ),
                            json=sre,
                            auth=(
                                self._open_search_user_name,
                                self._open_search_user_password,
                            ),
                            verify=False,
                        )
                        event = self._return_event(test_object, predetector_id)
                        requests.post(
                            (
                                f"{opensearch_destination}"
                                f"{self._opensearch_destination_event_index}/_doc"
                            ),
                            json=event,
                            auth=(
                                self._open_search_user_name,
                                self._open_search_user_password,
                            ),
                            verify=False,
                        )

    def _return_event(self, test_object, predetector_id):
        test_object["pre_detection_id"] = predetector_id
        test_object["execution_id"] = self._execution_id
        return test_object

    def _return_sre(self, predetector_id):
        return {
            "description": "Detects something",
            "id": self._rule_id,
            "title": "some title",
            "severity": "high",
            "mitre": ["attack.initial_access", "attack.t1078.002"],
            "case_condition": "selection",
            "rule_filter": '(data_stream.dataset:"something" AND event.code:"some")',
            "pre_detection_id": predetector_id,
            "@timestamp": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.000Z"),
        }

    def check_opsensearch_connection(self) -> bool:
        opensearch_request = (
            f"{self._opensearch_destination_host}:"
            f"{self._opensearch_destination_port}"
        )
        try:
            with warnings.catch_warnings():
                warnings.filterwarnings(
                    "ignore", category=urllib3.exceptions.InsecureRequestWarning
                )
                response = requests.get(
                    opensearch_request,
                    auth=(self._open_search_user_name, self._open_search_user_password),
                    verify=False,
                )
                requests.put(
                    f"{opensearch_request}/event",
                    auth=(self._open_search_user_name, self._open_search_user_password),
                    verify=False,
                )
                requests.put(
                    f"{opensearch_request}/event/_mapping",
                    auth=(self._open_search_user_name, self._open_search_user_password),
                    json=self._event_mapping(),
                    verify=False,
                )
                requests.put(
                    f"{opensearch_request}/sre",
                    auth=(self._open_search_user_name, self._open_search_user_password),
                    verify=False,
                )
                requests.put(
                    f"{opensearch_request}/sre/_mapping",
                    auth=(self._open_search_user_name, self._open_search_user_password),
                    json=self._sre_mapping(),
                    verify=False,
                )
                if response.status_code == 200:
                    return True
                else:
                    return False
        except Exception:
            return False

    @property
    def delay(self) -> float:
        """getter for delay

        Returns
        -------
        float
            returns a delay to write into OpenSearch as float
        """
        return self._data_delay

    @property
    def sre_to_create(self) -> int:
        """getter for sre_to_create

        Returns
        -------
        int
            returns the number of documents to be included in the sre and event index in OpenSearch
        """
        return self._data_sre_to_create

    @property
    def execution_id(self) -> str:
        """getter for execution_id

        Returns
        -------
        str
            returns a uuid as string to represent the unique execution
        """
        return self._execution_id

    @property
    def data_json_dict(self) -> dict:
        """getter for _data_dict

        Returns
        -------
        dict
            returns a dict object as representation of the json POST via http
        """
        return self._data_json_dict

    def _event_mapping(self) -> dict:
        return {
            "properties": {
                "execution_id": {"type": "keyword"},
                "pre_detection_id": {"type": "keyword"},
            }
        }
        return mapping

    def _sre_mapping(self) -> dict:
        return {
            "properties": {
                "execution_id": {"type": "keyword"},
                "pre_detection_id": {"type": "keyword"},
            }
        }
