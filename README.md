# ZEUS mock

## Getting started

### Bash on linux

```bash
python3 -m venv .venv # maybe you need to use "python" instead of "python3"
source ./.venv/bin/activate
pip install -r requirements.txt
pip install -r requirements_dev.txt
```

### Cmd on windows

```cmd
python3 -m venv .venv # maybe you need to use "python" instead of "python3"
.\.venv\Scripts\activate
pip install -r requirements.txt
pip install -r requirements_dev.txt
```

Before running, docker-compose `sysctl -w vm.max_map_count=262144` must be executed on the host system. On Windows you can change it in `Docker for Desktop`.

### Run

```bash
docker-compose -f docker-compose.yaml up
```

## Config

OpenSearch needs an admin password set at startup. This is set in the docker-compose file. Get it from there if you need it somewehere else or change it as you please.

## Usage

Required data:

- execution_id -> str
- rule_id -> str
- test -> json

### Example

To create 1 or 3 (random) sre per exection_id with a delay of 0-15 seconds (random), use:

```json
[
  {
    "execution_id": "exec#1",
    "rule_id": "rule#1",
    "test_events": [
      {
        "test_key1": "test_value1"
      },
      {
        "test_key2": "test_value2"
      }
    ]
  },
  {
    "execution_id": "exec#2",
    "rule_id": "rule#1",
    "test_events": [
      {
        "test_key3": "test_value3"
      },
      {
        "test_key4": "test_value4"
      }
    ]
  }
]
```

If you want to change this random parameter, you can add the following "mock" object to your test execution:

```json
[
  {
    "execution_id": "exec#2",
    "rule_id": "rule#1",
    "test_events": [
      {
        "test_key3": "test_value3"
      },
      {
        "test_key4": "test_value4"
      }
    ],
    "mock": {
      "sre_to_create": 2,
      "delay": 4
    }
  }
]
```

# Expected result

You get an event into the OpenSearch sre index (sre = security relevant event) and a corresponding event into an "event" index. They are linked via pre_detection_id (uuid). Sre and the use of a pre_detection_id is the same as you would find in zeus.

If you have change the OpenSearch credentials keep in mind to change them in requests, too.

To read all created documents in OpenSearch sre index, use:

```bash
curl -XGET -u 'admin:Admin123-admin!' 'https://<your_docker_ip>:9200/sre/_search' -d '{ "query": { "match_all": {} } }' -H 'Content-Type: application/json' -k
```

The same applies to the event index:

```bash
curl -XGET -u 'admin:Admin123-admin!' 'https://<your_docker_ip>:9200/event/_search' -d '{ "query": { "match_all": {} } }' -H 'Content-Type: application/json' -k
```

# Upgrade python dependencies

Please never modifiy the `requirements*.txt` files directly. When working with `pip-tools` the `requirements*.txt` files act as dependency lock files like in the javascript ecosystem.

First find out how [pip-tools](https://pypi.org/project/pip-tools/) works.
If `pip-compile` finds an existing `requirements.txt` file that fulfils the dependencies then no changes will be made, even if updates are available. To compile from scratch, first delete the existing `requirements*.txt` files.

```bash
rm requirements.txt requirements_dev.txt
```

Make your changes (e.g. add/remove a package or update version constraints) to the files (`requirements.in` and/or `requirements_dev.in`) and then compile from scratch.

```bash
pip-compile requirements.in
pip-compile requirements_dev.in
```

or upgrade a single package

```bash
pip-compile --upgrade-package Django
```

or

```bash
pip-compile --upgrade --upgrade-package 'Django<5'
```
