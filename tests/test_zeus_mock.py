import pytest
from zeus_mock.zeus_mock import ZeusMock


class TestZeusMock:
    @pytest.fixture
    def zeus_mock(self) -> ZeusMock:
        data = {
            "mock": {
                "sre_to_create": 5,
                "delay": 2
            },
            "execution_id": 123,
            "rule_id": 1
        }
        return ZeusMock(data)

    def test_validate_data_correct_json(self):
        testdata = {
            "mock": {
                "sre_to_create": 1,
                "delay": 5
            },
            "execution_id": 123,
            "rule_id": 1,
            "test_events": [
                {
                    "test_key1": "test_value1"
                }
            ]
        }
        zeus_mock = ZeusMock(testdata)
        assert zeus_mock.validate_data() == (True, "")

    def test_validate_data_missing_sre_to_create_key(self):
        testdata = {
            "mock": {
                "delay": 5
            },
            "execution_id": 123,
            "rule_id": 1,
            "test_events": [
                {
                    "test_key1": "test_value1"
                }
            ]
        }
        zeus_mock = ZeusMock(testdata)
        assert zeus_mock.validate_data() == (True, "")
        assert zeus_mock._data_json_dict["mock"]["sre_to_create"] in [1, 2]

    def test_validate_data_missing_sdelay_key(self):
        testdata = {
            "mock": {
                "sre_to_create": 1
            },
            "execution_id": 123,
            "rule_id": 1,
            "test_events": [
                {
                    "test_key1": "test_value1"
                }
            ]
        }
        zeus_mock = ZeusMock(testdata)
        assert zeus_mock.validate_data() == (True, "")
        assert zeus_mock._data_json_dict["mock"]["delay"] <= 15

    def test_delay(self, zeus_mock):
        zeus_mock.validate_data()
        assert zeus_mock.delay == 2

    def test_sre_to_create(self, zeus_mock):
        zeus_mock.validate_data()
        assert zeus_mock.sre_to_create == 5

    def test_data_json_dict(self, zeus_mock):
        zeus_mock.validate_data()
        assert isinstance(zeus_mock.data_json_dict, dict)

    def test_return_sre(self, zeus_mock):
        predetector_id = "550e8400-e29b-11d4-a716-446655440000"
        result = zeus_mock._return_sre(predetector_id)
        assert result["pre_detection_id"] == predetector_id
        assert result["id"] == zeus_mock._rule_id
        assert result["description"] == "Detects something"
        assert result["title"] == "some title"
        assert result["severity"] == "high"
        assert result["mitre"] == ["attack.initial_access", "attack.t1078.002"]
        assert result["case_condition"] == "selection"
        assert result["rule_filter"] == "(data_stream.dataset:\"something\" AND event.code:\"some\")"