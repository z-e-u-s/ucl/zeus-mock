ARG PYTHON_VERSION=3.9

FROM python:${PYTHON_VERSION}

WORKDIR /zeus-mock
COPY requirements.txt requirements.txt
RUN python3 -m pip install --no-cache-dir -r requirements.txt
COPY . .
EXPOSE 8000
CMD ["python", "-u", "run_zeus_mock.py"]

